/*
 * Copyright 2011 Benjamin Leperchey
 * Copyright 2014-2016 Tormod Volden
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "xsvf.h"
#include "xsvf-uart.h"
#include "cdcacm.h"

int sdr_bytes;

#define READ(m) for (i=0;i<m;i++) { buffer[n++] = usb_getchar(); }
//#define READ(m) for (i=0;i<m;i++) { buffer[n++] = fgetc(stream); }

int load_next_instr(uint8_t *buffer)
{
	int i,n;

	n = 0;
	READ(1);
	switch (buffer[0]) {
	case XTDOMASK:
		READ(sdr_bytes);
		break;
	case XREPEAT:
		READ(1);
		break;
	case XRUNTEST:
		READ(4);
		break;
	case XSIR:
		READ(1);
		READ(buffer[1]>>3);;
		break;
	case XSDR:
		READ(sdr_bytes);
		break;
	case XSDRSIZE:
		READ(4);
		sdr_bytes = 0;
		sdr_bytes |= (buffer[3])<<8;
		sdr_bytes |= (buffer[4]);
		sdr_bytes = (sdr_bytes+7)>>3;
		break;
	case XSDRTDO:
		READ(sdr_bytes);
		READ(sdr_bytes);
		break;
	case XSDRB:
		READ(sdr_bytes);
		break;
	case XSDRC:
		READ(sdr_bytes);
		break;
	case XSDRE:
		READ(sdr_bytes);
		break;
	case XSDRTDOB:
		READ(sdr_bytes);
		READ(sdr_bytes);
		break;
	case XSDRTDOC:
		READ(sdr_bytes);
		READ(sdr_bytes);
		break;
	case XSDRTDOE:
		READ(sdr_bytes);
		READ(sdr_bytes);
		break;
	case XSETSDRMASKS:
		READ(sdr_bytes);
		READ(sdr_bytes);
		break;
	case XCOMPLETE:
		break;
	case XSTATE:
		READ(1);
		break;

	/* pseudo instr to sync */
	case PING:
		break;

	case XSDRINC:
	default:
		return -1;
	}
	return n;
}
