/*
 * file: xsvf-acm.c
 *
 * Copyright 2011 Benjamin Leperchey
 * Copyright 2014-2016 Tormod Volden
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "xsvf-uart.h"
#include "xsvf.h"
#include "cdcacm.h"

unsigned char buffer[0x100];
int buffer_pos;
int buffer_size;

void uart_init(void)
{
	buffer_pos = 0;
	buffer_size = 0;
}

uint8_t read_next_instr()
{
	int n = 0;
	LOG_DEBUG("requesting next instruction");
	usb_putchar('+');
	while (n<=0) {
		n = load_next_instr(buffer);
	}
	LOG_DEBUG("received %d bytes",n);
	buffer_pos = 1;
	buffer_size = n;
	return buffer[0];
}

void fail(void)
{
	usb_putchar('f');
}

void success(void)
{
	usb_putchar('s');
}

#if 0
int get_hex_value(char c)
{
	if (c>='0' && c<='9') {
		return c-'0';
	} else if (c>='a' && c<='f') {
		return c-'a'+10;
	} else if (c>='A' && c<='F') {
		return c-'A'+10;
	} else {
		return -1;
	}
}
#endif

static int read_chunk(void)
{
	char c;
	unsigned char size;

	usb_putchar('+');
	
	c = usb_getchar();
	if (c!='+') {
		return 1;
	}

	size = usb_getchar()&0xff;

	buffer_size = 0;
	while (buffer_size<size) {
		buffer[buffer_size++] = usb_getchar();
	}
	LOG_DEBUG("read ok %02x",buffer_size);
	return 0;
}

int read_byte(uint8_t *data)
{
	if (buffer_pos>=buffer_size) {
		LOG_DEBUG("need more data");
		if (read_chunk()) {
			LOG_DEBUG("failed to get more data");
			return 1;
		}
		LOG_DEBUG("got more data");
		buffer_pos = 0;
	}
	*data = buffer[buffer_pos];
	buffer_pos++;
	return 0;
}

int read_word(uint16_t *data)
{
	uint8_t l,h;
	if (read_byte(&h)) {
		return 1;
	}
	if (read_byte(&l)) {
		return 1;
	}
	((uint8_t*)data)[0] = l;
	((uint8_t*)data)[1] = h;
	return 0;
}

int read_long(uint32_t *data)
{
	uint16_t l,h;
	if (read_word(&h)) {
		return 1;
	}
	if (read_word(&l)) {
		return 1;
	}
	((uint16_t*)data)[0] = l;
	((uint16_t*)data)[1] = h;
	return 0;
}

int read_bytes(uint8_t *data, int len)
{
	int i;
	int ret;
	for (i=len-1; i>=0; --i) {
		ret = read_byte(&data[i]);
		if (ret)
			break;
	}
	return ret;
}


