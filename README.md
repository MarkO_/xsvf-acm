# xsvf-acm

xsvf-acm is an XSVF player for programming e.g. Xilinx CPLDs. It runs
on an STM32F1 microcontroller, which when connected to a host computer
will appear as a USB CDC-ACM device (aka Virtual Serial Port).

On the host computer you will need to run the XSVF player client
from https://github.com/ben0109/XSVF-Player

The targeted microcontroller device is the Maple Mini, and with the
following pinout:

TCK = PA0 = Maple pin 11
TDO = PA1 = Maple pin 10
TDI = PA2 = Maple pin 9
TMS = PA3 = Maple pin 8

To use other pins, modify ports.c

The XSVF player code was ported from Ben's Papilio code, originally
for another project of mine which uses the serial port of the
microcontroller to communicate with the host computer:
https://gitlab.com/tormod/xsvf-player-stm32

The USB CDC ACM code is based on the usb_cdcacm example from
libopencm3 at https://github.com/libopencm3/libopencm3-examples

To build, install libopencm3 and tell make where you put it:
make OPENCM3_DIR=~/libopencm3 cdcacm.bin

Copyright 2016-2019 Tormod Volden

